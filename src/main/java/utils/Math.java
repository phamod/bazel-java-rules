package utils;

public class Math {
    public String test = "hello";
    private Math() {}

    public static int power(int x, int y) {
        int power = 1;
        for(int i = 0; i < y; i ++) {
            power *= x;
        }
        return power;
    }
}
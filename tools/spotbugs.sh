#/bin/bash

export SPOTBUGS_HOME=external/spotbugs
output=$1
target_jar=$2
jvmArgs=$3
classpath=$4
shift 4

#bazel info output_base >> $output
echo "hello world" > $output
echo "$@" >> $output
ls >> $output
echo $PATH >> $output
echo $jvmArgs >> $output
echo $SPOTBUGS_HOME/bin/spotbugs -textui -jvmArgs "$jvmArgs" -low -xml:withMessages -output $output $target_jar >> $output
$SPOTBUGS_HOME/bin/spotbugs -textui -jvmArgs "$jvmArgs" -auxclasspath "$classpath" -low -xml:withMessages -output $output $target_jar

bugs=$(grep -im 1 total_bugs= $output | awk -F'total_bugs=' '{print $2}' | awk -F'"' '{print $2}')

# if [ $bugs -gt 0 ]; then
#    echo "exiting bugs were found in the program"
#    exit 1
# fi
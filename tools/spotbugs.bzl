
"""Execute a binary.
The example below executes the binary target "//actions_run:merge" with
some arguments. The binary will be automatically built by Bazel.
The rule must declare its dependencies. To do that, we pass the target to
the attribute "_merge_tool". Since it starts with an underscore, it is private
and users cannot redefine it.
"""

def _impl(ctx):
    # The list of arguments we pass to the script.
    target_jar = ""
    classpath = ""
    for file in ctx.files.deps:
        classpath += file.path + ":"

    for jar in ctx.files.target_jars:
        if jar.extension == "jar":
            target_jar = jar
    

    args = [ctx.outputs.outfile.path]

    jvmArgs =  ctx.attr.jvmArgs
    if ctx.files.modules:
        jvmArgs += " --module-path "
        for file in ctx.files.modules:
            print(file.dirname)
            jvmArgs += file.path + ":"
     
    args += [target_jar.path] + [jvmArgs] + [classpath]

    # Action to call the script.
    ctx.actions.run(
        inputs = ctx.files.chunks +  [target_jar] + ctx.files.modules + ctx.files.deps,
        outputs = [ctx.outputs.outfile],
        arguments = args,
        progress_message = "running spotbugs to generate file %s" % ctx.outputs.outfile.short_path,
        executable = ctx.executable.spotbugs_script,
    )

concat = rule(
    implementation = _impl,
    attrs = {
        "chunks": attr.label_list(allow_files = True),
        "jvmArgs": attr.string(),
        "modules": attr.label_list(allow_files = True),
        "srcpath": attr.string(),
        "deps": attr.label_list(allow_files = True),
        "outfile": attr.output(mandatory = True),
        "target_jars": attr.label_list(allow_files = True),
        "spotbugs_script": attr.label(
            executable = True,
            cfg = "exec",
            allow_files = True,
            default = Label("//tools:spotbugs"),
        ),
    },
)
